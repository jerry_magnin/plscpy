#! /usr/bin/python3
# -*-coding:Utf-8 *-

#
# plscpy.py
# This file is part of plscpy
#
# Copyright (C) 2015 - Jerry Magnin
#
# plscpy is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# plscpy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with plscpy. If not, see <http://www.gnu.org/licenses/>.
#/

import shutil
import getopt
import sys
import os

def usage():
    helpText="""Programme de copie de contenu des pistes d'une playlist dans un seul dossier.
Écrit en Python.
Utilisation:
./plscpy.py [-v] (-f |--file=)playlist.pls (-d |--destination=)dossierDeDestination
    """
    print(helpText)

# On extrait les paramètres de la ligne de commande.
try:
    opts,args=getopt.getopt(sys.argv[1:],"f:d:v",["file=","destination="])
except getopt.GetoptError as err:
    print(err)
    usage()
    sys.exit(2)

filename = None
destination = None
verbose = False

for option,arg in opts:
    if option == "-v":
        # Starts verbose mode
        verbose = True
    elif option in ("-f","--file"):
        # Sets playlist file
        if os.path.exists(arg) and not os.path.isdir(arg):
            filename = arg
        else:
            print("Error: file {} does not exists".format(arg))
    elif option in ("-d","--destination"):
        # Sets destination folder. If it does not exists, it's created
        if os.path.exists(arg) and os.path.isdir(arg):
            destination = arg
        elif os.path.exists(arg):
            print("Error: {} is not a folder".format(arg))
            sys.exit(2)
        else:
            if verbose == True:
                print("Creating folder {}".format(arg))
            os.makedirs(arg)
            destination = arg
    else:
        print("Unknown argument {}".format(o))
        sys.exit(2)

if filename == None or destination == None:
    print("Error: missing parameters")
    usage()
    sys.exit(2)

nameFile = open(filename,"r")
names = nameFile.read()

splittedNames = names.splitlines()

for fileToCopy in splittedNames:
    if fileToCopy[0] != "#":
        if verbose == True:
            print("Copying {} into {}".format(fileToCopy, destination))
        shutil.copy(fileToCopy, destination)

