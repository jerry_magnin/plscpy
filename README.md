## À propos de plscpy
plscpy est un utilitaire permettant de copier toutes les pistes d’une playlist dans un dossier, à partir du fichier de sauvegarde de cette liste.
Il permet en fait un peu plus que ça, en exécutant une copie de tous les fichiers listés dans un fichier texte vers un dossier unique, mais son nom vient du fait que je l’ai initialement développé pour exporter mes playlists.

## Installation
### GNU/Linux
Ouvrez un terminal, et tapez les commandes suivantes.
```Bash
git clone https://git.framasoft.org/Rupicapra_rupicapra/plscpy.git plscpy
cd plscpy
sudo make install
```

## Utilisation
plscpy s’utilise en ligne de commande.   
Il accepte les options suivantes:
+ `-v` -> active le mode verbeux, dans lequel plscpy détaille ses actions ;
+ `-f playlist` ou `--file=playlist`, qui permet de donner le nom de la playlist **Cette option est obligatoire** ;
+ `-d dossier` ou `--destination=dossier`, pour fixer le dossier de destination **Cet argument est obligatoire**.

Les playlist au format texte sont acceptées comme fichiers d’entrée (.pls,.m3u). En fait, tout fichier texte peut être utilisé, et pas seulement pour copier une playlist.
Si le dossier de destination n’existe pas, il sera créé.

## À venir
Une meilleure organisation du code, l’option `--help`.
